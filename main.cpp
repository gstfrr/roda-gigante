// main.cpp - Wavefront OBJ viewer application

// V-ART example: Load and Display a OBJ file.

// This application shows how to create an object from a Wavefront OBJ file.

// Made from picker example, keeps the piking code.

// Changelog
// Oct 19, 2012 - Bruno de Oliveira Schneider
// - Adapted to new keyboard handler methods.
// Sep 26, 2012 - Bruno de Oliveira Schneider
// - Application created.

#include <vart/scene.h>
#include <vart/box.h>
#include <vart/cylinder.h>
#include <vart/light.h>
#include <vart/meshobject.h>
#include <vart/sphere.h>
#include <vart/contrib/viewerglutogl.h>
#include <vart/contrib/mousecontrol.h>

#include <iostream>

using namespace std;

// Define the keyboard handler
class KeyboardHandler : public VART::ViewerGlutOGL::KbHandler
{
    public:
        KeyboardHandler() {
        }
        virtual void OnKeyDown(int key) {
            switch (key)
            {
                case 'j':
                    break;
                case 'l':
                    break;
                case 'i':
                    break;
                case 'k':
                    break;
            }
        }
    private:
};

class MyIHClass : public VART::ViewerGlutOGL::IdleHandler
{
    public:
        MyIHClass() : redRadians(0) {}
        virtual ~MyIHClass() {}
        virtual void OnIdle() {
            redRadians += 0.01;
            redRotPtr->MakeXRotation(redRadians);
            viewerPtr->PostRedisplay();
        }
    //protected:
        VART::Transform* redRotPtr;
    private:
        float redRadians;
};

class ClickHandlerClass : public VART::MouseControl::ClickHandler
{
    public:
        ClickHandlerClass() {
        }
        virtual ~ClickHandlerClass() {};
        virtual void OnClick() {
            if (mouseCtrlPtr->LastClickIsDown()) {
                VART::MouseControl::ButtonID lastButton = mouseCtrlPtr->GetLastClickButton();

                if (lastButton == VART::MouseControl::LEFT) {
                    scenePtr->Pick(mouseCtrlPtr->GetClickXPosition(),
                                   mouseCtrlPtr->GetClickYPosition(), &pickList);
                    if (!pickList.empty()) {
                        cout << pickList.size() << " selected objects: ";
                        while (!pickList.empty()) {
                            cout << pickList.front()->GetDescription() << ":\n";
#ifdef DEBUG
                            VART::MeshObject* ptObj = dynamic_cast<VART::MeshObject*>(pickList.front());
                            if (ptObj)
                                cout << *ptObj << "\n";
#endif
                            pickList.pop_front();
                        }
                        cout << endl;
                    }
                }
            }
        }
        list<VART::GraphicObj*> pickList;
        VART::Scene* scenePtr;
};

// The application itself:
int main(int argc, char* argv[])
{
    VART::ViewerGlutOGL::Init(&argc, argv); // Initialize GLUT

    static VART::Scene scene; // create a scene
    static VART::ViewerGlutOGL viewer(700,700); // create a viewer (application window)

    KeyboardHandler kbh; // create a keyboard handler
    // create a camera (scene observer)
    VART::Camera camera(VART::Point4D(200,80,0),VART::Point4D(0,0,0),VART::Point4D::Y());

    camera.SetFarPlaneDistance(400);
    
    // create some objects
    list<VART::MeshObject*> objects;
    ClickHandlerClass clickHandler;
    
    MyIHClass idleHandler;

    clickHandler.scenePtr = &scene;

    // Initialize scene objects
    VART::MeshObject::ReadFromOBJ("ferris-wheel.obj", &objects);
    VART::Transform translacaoCadeira;
    VART::Transform rotacaoCadeira;
    VART::Transform rodacao;
    VART::Transform rotacao[10];


    //Transofrmacoes
    translacaoCadeira.MakeTranslation(0,0,70);

    // Build up the scene
    list<VART::MeshObject*>::iterator iter = objects.begin();

    VART::MeshObject* cadeiras[10];
    rotacaoCadeira.MakeIdentity();

    for(int i=0;i<10;++i){
        cadeiras[i] = dynamic_cast<VART::MeshObject*>((*iter)->Copy());
        //cadeiras[i] = *iter;

        cadeiras[i]->SetMaterial(VART::Color::BLUE());

        translacaoCadeira.AddChild(*cadeiras[i]);

        rotacao[i].MakeIdentity();
        rotacao[i].AddChild(translacaoCadeira);
        rotacao[i].MakeXRotation(i*0.63);

        rotacaoCadeira.AddChild(rotacao[i]);

        //rotacaoCadeira.MakeXRotation(0.65);
    }

    ++iter;
    VART::MeshObject* base = *iter;
    base->SetMaterial(VART::Color::GREEN());

    ++iter;
    VART::MeshObject* roda = *iter;
    roda->SetMaterial(VART::Color::YELLOW());

    rotacaoCadeira.AddChild(*roda);
    scene.AddObject(base);
    scene.AddObject(&rotacaoCadeira);

    scene.AddLight(VART::Light::BRIGHT_AMBIENT());
    scene.AddCamera(&camera);
    //scene.MakeCameraViewAll();

    idleHandler.redRotPtr = &rotacaoCadeira;

    // Set up the viewer
    viewer.SetTitle("V-ART OBJ Viewer");
    viewer.SetScene(scene); // attach the scene
    viewer.SetKbHandler(&kbh); // attach the keyboard handler
    viewer.SetClickHandler(&clickHandler);
    viewer.SetIdleHandler(&idleHandler);

    // Run application
    scene.DrawLightsOGL(); // Set OpenGL's lights' state
    VART::ViewerGlutOGL::MainLoop(); // Enter main loop (event loop)
    return 0;
}
